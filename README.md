# Benchmark instances and detailed results for "A MIP backend for IDP" #

* /Problems/MIP contains direct MIP specifications, either in the form of .cpp files compilable with CPLEX, or .lp.gz files representing a MIP model.
* /Problems/Modelexpand contains IDP specifications and instances for search problems.
* /Problems/Optimize contains IDP specifications and instances for optimization problems.
* .csv files contain detailed experimental data. 