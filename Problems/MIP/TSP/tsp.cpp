/**
* TSP
*/

#include <fstream>
#include <string>
#include <ilcplex/ilocplex.h>
using namespace std;

ILOSTLBEGIN

typedef IloArray<IloIntArray>    IntMatrix;
typedef IloArray<IloNumArray>    NumMatrix;
typedef IloArray<IloNumVarArray> NumVarMatrix;

const IloInt Horizon = 100000;

int main(int argc, char** argv)
{
	IloEnv env;
	try {
		IloInt i, j;

		string filein;
		if (argc > 1)
			filein = string(argv[1]);
		else
			filein = "test.dat";
		ifstream f(filein.c_str(), ios::in);
		if (!f) {
		   cerr << "No such file: " << filein << endl;
		   throw(1);
		}

		bool onlystats = argc>2;

		//******************************************************************
		IloNumArray cities(env);
		IloNum capacity;
		IloArray<IloIntArray> distantMatrix(env);

		f>>distantMatrix;
		IloInt nbCities = distantMatrix.getSize();
		IloModel model(env);

		// Create variables x[i][j] for all 0 <= j < i < n. x[i][j] = 1 indicates that the edge (i,j) is part of the tour.
		IloArray<IloIntVarArray>  x(env, nbCities);
		for (j = 0; j < nbCities; j++) {
			x[j] = IloIntVarArray(env, nbCities, 0, 1);
		}
		IloNumVarArray u(env, nbCities, 0.0, 10000, ILOFLOAT);

		// degree constraints
		for (i = 0; i < nbCities; i++) {
			IloExpr sum1(env);
			IloExpr sum2(env);
		for (j = 0; j < nbCities; j++){
			sum1 += x[i][j];
			sum2 += x[j][i];
		}
		model.add(sum1 == 1);
		model.add(sum2 == 1);
		//diagonal constraints
		model.add(x[i][i] == 0);
		}
		//subtour elimination constraints
	for(i = 1;i<nbCities;i++)
	{
		for(j = 1;j < nbCities; j++){
			if(i != j){
				IloExpr sec(env);
				sec += u[i];
				sec -= u[j];
				sec += nbCities*x[i][j];
				model.add(sec <= nbCities - 1);
			}
		}
	}
	model.add(u[0]==1);


	// Objective is to minimize the sum of starting time
	IloExpr obj(env);
	for (i = 0; i < nbCities; i++)
		for(j = 0;j<nbCities;j++)
			obj += distantMatrix[i][j]*x[i][j];

	model.add(IloMinimize(env, obj));

	IloCplex cplex(env);
	cplex.extract(model);
	std::cout << "Number of rows: " << cplex.getNrows() << std::endl;
	std::cout << "Number of columns: " << cplex.getNcols() << std::endl;

	if(onlystats){
		return 0;
	}

	if (cplex.solve()) {
		//cout<<fileout<<endl;
		  cout << "Solution status: " << cplex.getStatus() << endl;
		  cout << "Optimal Value = " << cplex.getObjValue() << endl;
	//          cout << "number of cities: "<<nbCities<<endl;
	//          std::cout<<std::endl;

	//          for (i = 0; i < nbCities; i++){
	//        	  for (j = 0; j < nbCities; j++){
	//        		  if(cplex.getValue(x[i][j]) < 0.00001)
	//        			  std::cout<<"\t"<<0;
	//        		  else
	//        			  std::cout<<"\t"<<1;
	//        	  }
	//        	  std::cout<<std::endl;
	//          }
	//          std::cout<<std::endl;
	}
	else
		cout<<"oops, something's wrong @_@\n";
	}
	catch (IloException& ex) {
		cerr << "Error: " << ex << endl;
	}
	catch (...) {
		cerr << "Error" << endl;
	}
	env.end();
	return 0;
}
