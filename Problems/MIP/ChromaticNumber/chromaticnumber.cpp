/**
* Chromatic Number
*/

#include <fstream>
#include <string>
#include <ilcplex/ilocplex.h>
using namespace std;

ILOSTLBEGIN

typedef IloArray<IloIntArray>    IntMatrix;
typedef IloArray<IloNumArray>    NumMatrix;
typedef IloArray<IloNumVarArray> NumVarMatrix;

const IloInt Horizon = 100000;

int main(int argc, char** argv)
{
   IloEnv env;
   try {
      IloInt i, j;

      string filein;
      if (argc > 1)
    	  filein = string(argv[1]);
      else
    	  filein = "test.dat";
      ifstream f(filein.c_str(), ios::in);
      if (!f) {
         cerr << "No such file: " << filein << endl;
         throw(1);
      }
			bool onlystats=argc > 2;

      /* input structure
       10			//number of nodes
       10			//number of colors
       15			//number of edges
       //edges
       1 2
       5 7
       */
      //******************************************************************
      IloNumArray nodes(env);
      int nbNodes;
      int nbColors;
      int nbEdges;
      int from, to;
      IloIntArray edgeFrom(env);
      IloIntArray edgeTo(env);

      f>>nbNodes>>nbColors>>nbEdges;
      cout<<nbNodes<<" "<<nbColors<<" "<<nbEdges<<endl;
      for(int i = 0;i<nbEdges;i++)
      {
    	  f>>from>>to;
    	  //cout<<from<<" "<<to<<endl;
    	  edgeFrom.add(from-1);
    	  edgeTo.add(to-1);
      }
//      IloInt nbCities = distantMatrix.getSize();
      IloModel model(env);

      // Create variables x[i][k]
      IloArray<IloIntVarArray>  x(env, nbNodes);
      for (int i = 0; i < nbNodes; i++) {
    	  x[i] = IloIntVarArray(env, nbColors, 0, 1);
      }
      //create avariable y[k] representing color k is used or not
      IloIntVarArray y(env, nbColors, 0, 1);

      for(int i = 0;i<nbNodes;i++){
    	  IloExpr sumColorForNode(env);
    	  for(int k = 0;k<nbColors;k++){
    		  sumColorForNode += x[i][k];
    		  model.add(x[i][k] - y[k] <= 0);				//color k is used only if is assigned to at least 1 node
    	  }
    	  //each node is assign to 1 color only				sum_k x_ij = 1 for all i
//    	  model.add(sumColorForNode == 1);
    	  model.add(sumColorForNode >= 1);
      }

      for(int i = 0;i<nbEdges;i++){
    	  //cout<< edgeFrom[i] <<" "<<edgeTo[i]<<endl;
    	  for(int k = 0;k<nbColors;k++){
    		  model.add(x[edgeFrom[i]][k] + x[edgeTo[i]][k] <= 1);
    	  }
      }

      // Objective is to minimize the number of used color
      IloExpr obj(env);
      for (int k = 0; k < nbColors; k++)
    		  obj += y[k];
      model.add(IloMinimize(env, obj));

      IloCplex cplex(env);
      cplex.extract(model);
			std::cout << "Number of rows: " << cplex.getNrows() << std::endl;
			std::cout << "Number of columns: " << cplex.getNcols() << std::endl;

			if(onlystats){
				return 0;
			}

      if (cplex.solve()) {
          cout << "Solution status: " << cplex.getStatus() << endl;
          cout << "Optimal Value = " << cplex.getObjValue() << endl;
//          for(int i = 0;i<nbNodes;i++){
//        	  for(int k = 0;k<nbColors;k++)
//        		  if(cplex.getValue(x[i][k]) >= 1)
//        			  cout<<"node "<<i<<" "<<k<<endl;
//          }

      }
      else
    	  cout<<"oops, something's wrong @_@\n";
   }
   catch (IloException& ex) {
      cerr << "Error: " << ex << endl;
   }
   catch (...) {
      cerr << "Error" << endl;
   }
   env.end();
   return 0;
}
