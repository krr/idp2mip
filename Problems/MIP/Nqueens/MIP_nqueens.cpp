//============================================================================
// Name        : MIP_nqueens.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================



#include <ilcplex/ilocplex.h>
#include <iostream>
using namespace std;

// input is 1 file containing only the dimension of the n x n chess board
// second argument (if given) is a time limit

ILOSTLBEGIN

int readInputFile(const char* filename){
	ifstream f(filename, ios::in);
	if (!f)
		return 0;
	string x;
	getline(f, x);
	return(atoi(x.c_str()));
}


//main n timelimit
int main(int argc, char** argv) {
	int n = readInputFile(argv[1]);
	if(n==0){
		cerr << "Error reading file: " << argv[1] << endl;
		throw(1);
	}

	int timelimit = -1;
	if(argc >= 3)
		timelimit = atoi(argv[2]);

	IloEnv env;
	IloModel model(env);

	//create x[i][j] variable represent squares on the board
	IloArray<IloIntVarArray>  x(env, n);
	for (int i = 0;i<n;i++){
		x[i] = IloIntVarArray(env, n, 0, 1);
	}

	IloExpr obj(env);
	for(int i = 0;i<n;i++){
		//row and col
		IloExpr row(env);
		IloExpr col(env);
		for(int j = 0;j<n;j++){
			row += x[i][j];
			col += x[j][i];
		}
		model.add(row == 1);				//<= is fine since == 1 is ensured by maximize in objective function
		model.add(col == 1);				//however, == performs incredibly good
	}
	//diagonal
	//number of diagonal each type: 2n - 1
	for(int t = 1; t<= 2*n-1; t++)
	{
		IloExpr diag1(env);		//forward one
		IloExpr diag2(env);		//backward one
		for(int i = 0;i<n;i++)
			for(int j = 0;j<n;j++){
				if (i - j + n == t)
					diag1 += x[i][j];
				if (i + j +1 == t)
					diag2 += x[i][j];
			}
		model.add(diag1 <= 1);
		model.add(diag2 <= 1);
	}

	for(int i = 0;i<n;i++)
		for(int j = 0;j<n;j++)
			obj += x[i][j];
	model.add(IloMaximize(env, obj));

	IloCplex cplex(env);
	cplex.extract(model);
	std::cout << "Number of rows: " << cplex.getNrows() << std::endl;
	std::cout << "Number of columns: " << cplex.getNcols() << std::endl;

	if(timelimit != -1)
		cplex.setParam(IloCplex::TiLim,timelimit);
	if (cplex.solve()) {
		  cout << "Solution status: " << cplex.getStatus() << endl;
		  cout << "Optimal Value = " << cplex.getObjValue() << endl;
//		  for(int i = 0;i<n;i++){
//			  cout<<endl;
//			  for(int j = 0;j<n;j++){
//				  cout<<setw(2)<<cplex.getValue(x[i][j])<<" ";
//			  }
//		  }
	}
	else
		cout<<"st wrong, can not solve"<<endl;

	env.end();
	return 0;
}
