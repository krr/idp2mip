/**
* Towers of Hanoi
*/

#include <ilcplex/ilocplex.h>
#include <iostream>
#include <utility>
#include <vector>
#include <map>
using namespace std;

// NOTE: ASSUMING INPUT DISK NUMBER STARTS AT 1 AND ENDS AT N (given N disks)
// NOTE: ASSUMING INPUT TIME STARTS AT 0
// NOTE: ASSUMING INPUT DISK CAN STAND ON AT MOST ONE DISK

ILOSTLBEGIN

bool readInputFile(const char* filename, vector<int> &times, vector<int> &disk, map<int,int> &onO, map<int,int> &onGoal);

//main filename timelimit
int main(int argc, char** argv) {
	IloEnv env;

	const char* filename;
	if (argc >= 2)
		filename = argv[1];
	else
		filename = "test.dat";

	vector<int> times;
	map<int, int> onstart;
	map<int, int> ongoal;
	vector<int> disks;

	if(!readInputFile(filename, times, disks, onstart, ongoal))
	{
		cerr << "Error reading file: " << filename << endl;
		throw(1);
	}

	bool onlystats= argc>2;

	IloModel model(env);

	int nTimeSteps = times.size();
	int nDisks = disks.size();
	int finalstep = nTimeSteps-1;
	IloIntVar Move[nTimeSteps][nDisks][nDisks];
	IloIntVar On[nTimeSteps][nDisks][nDisks];

	for(int t = 0;t<nTimeSteps;t++)
		for(int d1 = 0;d1 < nDisks; d1++)
			for(int d2 = 0;d2 < nDisks; d2++)
			{
				Move[t][d1][d2] = IloIntVar(env, 0, 1);
				On[t][d1][d2] = IloIntVar(env, 0, 1);
			}

	//Minimize SUM{Move(t,d1,d2)}  (minimize the number of moves)
	IloExpr obj(env);
	for(int t = 0;t<nTimeSteps;t++)
			for(int d1 = 0;d1 < nDisks; d1++)
				for(int d2 = 0;d2 < nDisks; d2++)
			obj += Move[t][d1][d2];
	model.add(IloMinimize(env, obj));
	//model.add(IloMaximize(env, obj));


	for(int t = 0;t<nTimeSteps;t++)
	{
		//!t: SUM{Move(t,d1,d2)} =< 1 (we move at most one disk at a time)
		IloExpr moveonedisk(env);
		for(int d1 = 0;d1 < nDisks; d1++)
			for(int d2 = 0;d2 < nDisks; d2++)
				moveonedisk += Move[t][d1][d2];
		model.add(moveonedisk <= 1);

		for(int d1 = 0;d1 < nDisks; d1++){
			//!t d1: SUM{On(t,d1,d2)} =< 1 (a disk can stand on at most one disk)
			IloExpr standOnOneDisk(env);
			//!t d2: SUM{On(t,d1,d2)} =< 1 (at most one disk can stand on a disk)
			IloExpr oneDiskStandOn(env);
			for(int d2 = 0;d2 < nDisks; d2++){
				standOnOneDisk += Move[t][d1][d2];
				oneDiskStandOn += Move[t][d2][d1];
			}
			model.add(standOnOneDisk <= 1);
			model.add(oneDiskStandOn <= 1);
		}

		for(int d1 = 0;d1 < nDisks; d1++)
			for(int d2 = d1; d2<nDisks; d2++){
				//!t d1<=d2: On(t,d1,d2)=0 (a disk can not stand on an equal or smaller disk)
				model.add(On[t][d1][d2]==0);

				//preconditions on moving:
				//!t d1>=d2: Move(t,d1,d2)=0 (a disk can only be moved to a smaller disk)
				model.add(Move[t][d1][d2]==0);
			}

		for(int d1 = 0;d1 < nDisks; d1++)
			for(int d2 = 0;d2 < nDisks; d2++){
				// !t d1 d2: Move(t,d1,d2) => !d3: ~On(t,d3,d1)	(a disk can only be moved if there are no disks on top)
				IloExpr from_noDiskOnTop(env);

				//!t d1 d2: Move(t,d1,d2) => !d3: ~On(t,d3,d2)	a disk can only be moved to a disk with no disks on top
				IloExpr to_noDiskOnTop(env);

				for(int d3 = 0;d3 < nDisks; d3++)
				{
					from_noDiskOnTop += On[t][d3][d1];
					to_noDiskOnTop += On[t][d3][d2];
				}
				model.add(IloIfThen(env, Move[t][d1][d2] == 1, from_noDiskOnTop <= 0));
				model.add(IloIfThen(env, Move[t][d1][d2] == 1,  to_noDiskOnTop <= 0));

				//consequences of moving:
				//!t d1 d2: Move(t,d1,d2) =< On(t+1,d1,d2)	(moving a disk means it will be on the other disk the next timestep)
				if(t != nTimeSteps - 1){
					model.add(Move[t][d1][d2] <= On[t+1][d1][d2]);

					//!t d1 d2 d3 (d3!=d2): Move(t,d1,d2) =< 1-On(t+1,d1,d3)
					//(moving a disk means it will not be on another disk the next timestep)
					for(int d3 = 0;d3 < nDisks; d3++)
						if(d3 != d2)
							model.add(Move[t][d1][d2] <= 1-On[t+1][d1][d3]);

//					//consequences of not moving
//					//!t d1 d2: On(t+1,d1,d2)-On(t,d1,d2) =< Move(t,d1,d2).
					model.add(On[t+1][d1][d2] - On[t][d1][d2] <= Move[t][d1][d2]);

					IloExpr notmove(env);
					for(int d3 = 0;d3 < nDisks; d3++)
						notmove += Move[t][d1][d3];
					model.add(On[t][d1][d2] - On[t+1][d1][d2] <= notmove);
					//!t d1 d2: if On(t,d1,d2)-On(t+1,d1,d2) != 0 => !d3 ~Move(t,d1,d3).
				}
			}
	}

	//fix the On variables on the initial and the final timesteps
	for(int d1 = 0;d1 < nDisks; d1++){
		for(int d2 = 0;d2 < nDisks; d2++){
			if(onstart.count(d1)>0 && onstart[d1]==d2){
				model.add(On[0][d1][d2] == 1);
			}else{
				model.add(On[0][d1][d2] == 0);
			}
			if(ongoal.count(d1)>0 && ongoal[d1]==d2){
				model.add(On[finalstep][d1][d2] == 1);
			}else{
				model.add(On[finalstep][d1][d2] == 0);
			}
		}
	}

	IloCplex cplex(env);
	cplex.extract(model);
	std::cout << "Number of rows: " << cplex.getNrows() << std::endl;
	std::cout << "Number of columns: " << cplex.getNcols() << std::endl;

	if(onlystats){
		return 0;
	}

	cout<<"start solving...."<<endl;
	if (cplex.solve()) {
		cout<<"hey you..."<<endl;
		cout << "Solution status: " << cplex.getStatus() << endl;
		cout << "Optimal Value = " << cplex.getObjValue() << endl;
//		for(int t = 0;t<nTimeSteps;t++)
//		{
//			for(int d1 = 0;d1 < nDisks; d1++)
//				for(int d2 = 0;d2 < nDisks; d2++)
//				{
//					if(cplex.getValue(On[t][d1][d2]) >= 0.9)
//						cout<<"on "<<t<<" - ("<<d1 + 1<<", "<<d2 + 1<<")"<<endl;
//				}
//
//			for(int d1 = 0;d1 < nDisks; d1++)
//				for(int d2 = 0;d2 < nDisks; d2++)
//				{
//					if(cplex.getValue(Move[t][d1][d2]) >= 0.9)
//						cout<<"move "<<t<<" - ("<<d1 + 1<<", "<<d2 + 1<<")"<<endl;
//				}
//			cout<<endl;
//		}

	}
	else
		cout<<"st wrong, can not solve"<<endl;

	env.end();
	return 0;
}


bool readInputFile(const char* filename, vector<int> &times, vector<int> &disk, map<int,int> &onO, map<int,int> &onGoal)
{
	ifstream f(filename, ios::in);
	if (!f)
		return false;
	string x;
	getline(f, x);			//factlist S:FactListVoc{
	int t;

	while(1){
		getline(f,x);
		if(x.find("}") != std::string::npos)
			break;

		if(x.find("time") != string::npos){
			x = x.substr(5, x.length()-7);
			times.push_back(atoi(x.c_str()));
		}
		else if(x.find("disk") != string::npos){
			x = x.substr(5, x.length()-7);
			disk.push_back(atoi(x.c_str())-1);
		}
		else if(x.find("on0") != string::npos){
			x = x.substr(4, x.length()-6);
			t = x.find(",");
			if(t == std::string::npos)
				return false;
			int from = atoi(x.substr(0,t).c_str())-1;
			int to = atoi(x.substr(t+1, x.length()-t).c_str())-1;
			onO.insert(make_pair(from, to));
			//cout<<"on0: "<<from<<" "<<to<<endl;
		}
		else if(x.find("ongoal") != string::npos){
			x = x.substr(7, x.length()-9);
			t = x.find(",");
			if(t == std::string::npos)
				return false;
			int from = atoi(x.substr(0,t).c_str())-1;
			int to = atoi(x.substr(t+1, x.length()-t).c_str())-1;
			onGoal.insert(make_pair(from, to));
			//cout<<"ongoal: "<<from<<" "<<to<<endl;
		}
	}
	cout<<"read file successfully"<<endl;
	cout<<"number of disks: "<<disk.size()<<endl;
	cout<<"number of time steps: "<<times.size()<<endl;
	return true;
}

