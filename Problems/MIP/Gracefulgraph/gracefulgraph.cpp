/**
* Graceful Graph
*/

#include <ilcplex/ilocplex.h>
#include <iostream>
#include <utility>
#include <vector>
using namespace std;

#define GRAPHMAXVERTICES 100000

ILOSTLBEGIN

int insertVertex(int v, vector<int> &lvertices);
bool readInputFile(const char* filename, vector< pair<int,int> > &lEdges, vector<int> &lvertices);

//main n timelimit
int main(int argc, char** argv) {
	IloEnv env;
	try{
		const char* filename;
		if (argc >= 2)
			filename = argv[1];
    else
  	  filein = "test.dat";

		vector<pair<int, int> > listEdges;
		vector<int> listVertices;
		if(!readInputFile(filename, listEdges, listVertices))
		{
			cerr << "Error reading file: " << filename << endl;
			throw(1);
		}

		bool onlystats= argc>2;

		IloModel model(env);
		//vertex variable
		IloIntVarArray vertex = IloIntVarArray(env, listVertices.size(), 0, GRAPHMAXVERTICES);
		IloIntVarArray edge = IloIntVarArray(env, listEdges.size(), 1, listEdges.size());

//		for(int i = 0;i<listVertices.size()-1;i++)
//			for(int j = i+1;j<listVertices.size();j++)
//				model.add(vertex[i] != vertex[j]);


		for(int i = 0;i<listEdges.size();i++){
			for(int j = i+1;j<listEdges.size();j++)
				model.add(edge[i] != edge[j]);
			//label[e] = label[from_e] - label[to_e]
			pair<int, int> e = listEdges[i];

			model.add((vertex[e.first] - vertex[e.second] == edge[i]) ||
					(vertex[e.first] - vertex[e.second] == -1 * edge[i]));
		}


		IloNumVar dummy(env, 0, 0, ILOINT);
		model.add(IloMaximize(env, dummy));


		IloCplex cplex(env);
		cplex.extract(model);
		std::cout << "Number of rows: " << cplex.getNrows() << std::endl;
		std::cout << "Number of columns: " << cplex.getNcols() << std::endl;

		if(onlystats){
			return 0;
		}

		cout<<"start solving...."<<endl;
		if (cplex.solve()) {
			cout << "Solution status: " << cplex.getStatus() << endl;
			cout << "Optimal Value = " << cplex.getObjValue() << endl;
			for(int i = 0;i<listVertices.size();i++)
				cout<<listVertices[i]<<" - "<<cplex.getValue(vertex[i])<<endl;
			cout<<endl;
			cout<<"number of edges: "<<listEdges.size()<<endl;
			for(int i = 0;i<listEdges.size();i++){
				pair<int, int> e = listEdges[i];
				cout<<listVertices[e.first]<<" - "<<listVertices[e.second]<<": ";
				cout<<cplex.getValue(vertex[e.first])<<" - "<<cplex.getValue(vertex[e.second])<<" : ";
				cout<<cplex.getValue(edge[i])<<endl;
			}
		}
		else
			cout<<"st wrong, can not solve"<<endl;

	}
	catch (...) {
		cerr << "Error" << endl;
	}

	env.end();
	return 0;
}


bool readInputFile(const char* filename, vector< pair<int,int> > &lEdges, vector<int> &lvertices)
{
	ifstream f(filename, ios::in);
	if (!f)
		return false;
	string x;
	getline(f, x);

	while(1){
		getline(f,x);
		if(x.find("}") != std::string::npos)
			break;
		x = x.substr(5, x.length()-7);
		int t = x.find(",");
		if(t == std::string::npos)
			return false;
		int from = atoi(x.substr(0,t).c_str());
		int ifrom = insertVertex(from, lvertices);
		int to = atoi(x.substr(t+1, x.length()-t).c_str());
		int ito = insertVertex(to, lvertices);
		lEdges.push_back(make_pair(ifrom, ito));
	}
	return true;
}
int insertVertex(int v, vector<int> &lvertices)
{
	//return the index of the new vertex in the list
	int i= 0;
	for(;i<lvertices.size();i++)
		if(lvertices[i] == v)
			break;
	if(i == lvertices.size()){
		lvertices.push_back(v);
		return lvertices.size()-1;
	}
	else
		return i;
}
